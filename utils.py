from sklearn.decomposition import PCA
from imblearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix


def show_confusion_matrix(y_true,y_pred):
    cm = confusion_matrix(y_true,y_pred)
    ax= plt.subplot()
    sns.heatmap(cm, annot=True, fmt='g', ax=ax);  #annot=True to annotate cells, ftm='g' to disable scientific notation

    # labels, title and ticks
    ax.set_xlabel('Predicted labels')
    ax.set_ylabel('True labels')
    ax.set_title('Confusion Matrix')
    ax.xaxis.set_ticklabels(['Not Defaulter', 'Defaulter'])
    ax.yaxis.set_ticklabels(['Not Defaulter', 'Defaulter'])

def show_scatter_matrix(df) :
    sns.set_theme(style="ticks")

    label = df['Loan Status']

    start = 0
    end = 4
    jump = 4
    for i in range(0, 3):
        df_crop = df.iloc[:, start:end]
        df_crop_label = df_crop.join(label)
        start = end + 1
        end = start + jump
        sns.pairplot(df_crop_label, hue='Loan Status')


def show_class_distr(y) :
    labels = ['Not Defaulter', 'Defaulter']

    counts = [y[y==0].size, y[y==1].size]
    bar_colors = [ 'tab:blue', 'tab:red' ]

    plt.bar(labels, counts,color = bar_colors)
    plt.title("Class distribution")
    plt.show()
    print(counts)


def GS(X_train,y_train,names,classifiers,parameters,scoring,sampler,verbose=0) :
    assert len(names) == len(classifiers) == len(parameters)

    print("Starting Grid Search..")
    print(f"Scoring metrics : {scoring}")
    print("Classifiers : ",end = '')

    # printing classifiers..
    for i in range(len(classifiers)) :
        if i != len(classifiers)-1 :
            print(names[i],end=', ')
        else :
            print(names[i])

    results = []
    best_model = 0
    for i in range(len(classifiers)) :
        print('='*100)
        print('Running classifier : ' + names[i])

        gs = None
        pca = PCA()
        pipeline = Pipeline([('sampler', sampler), ('pca', pca), ('classifier', classifiers[i])])
        gs = GridSearchCV(pipeline, parameters[i], cv=3, n_jobs=-1, scoring=scoring, verbose=verbose).fit(X_train, y_train)
        print("Best params : ",gs.best_params_)
        print("Test score  : ",gs.best_score_)

        result = {}
        result['name'] = names[i]
        result['gs'] = gs
        result['score'] = gs.best_score_

        results.append(result)
        if results[best_model]['score'] < results[i]['score'] :
            best_model = i

    print('='*100)
    print()
    print(f"The best model is {results[best_model]['name']} with score {results[best_model]['score']}")

    return results[best_model]['gs']
